package com.survey.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CustomerSurvey1Application {

	public static void main(String[] args) {
		SpringApplication.run(CustomerSurvey1Application.class, args);
	}

}

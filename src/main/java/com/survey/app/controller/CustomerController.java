package com.survey.app.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.survey.app.dto.ApiResponse;
import com.survey.app.dto.CustomerDto;
import com.survey.app.service.CustomerService;

import jakarta.validation.Valid;

@RestController
@RequestMapping
public class CustomerController {
	private final CustomerService customerService;
	public CustomerController(CustomerService customerService) {
		
		this.customerService = customerService;
	}

	@PostMapping("/customer")

	public ResponseEntity<ApiResponse> addCustomer(@Valid @RequestBody CustomerDto customerDto) {
		return ResponseEntity.status(HttpStatus.CREATED).body(customerService.addcustomer(customerDto));
	}
}

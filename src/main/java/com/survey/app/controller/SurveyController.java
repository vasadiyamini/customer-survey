package com.survey.app.controller;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.survey.app.entity.Survey;
import com.survey.app.service.SurveyService;


@RestController
@RequestMapping("/survey")
public class SurveyController {
	private final SurveyService surveyService;

	public SurveyController(SurveyService surveyService) {
		this.surveyService = surveyService;
	}
	@GetMapping("/{branchId}")
	public ResponseEntity<List<Survey>> getCuss( 
			@PathVariable Integer branchId,@RequestParam Integer distance) {
		return ResponseEntity.status(HttpStatus.OK).body(
				surveyService.getCus( branchId ,distance));
	}
}

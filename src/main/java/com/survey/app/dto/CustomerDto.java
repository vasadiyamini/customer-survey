package com.survey.app.dto;


import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CustomerDto {
	@NotBlank(message="firstname required")
	private String firstName;
	private String lastName;
	@Email(message="Invalid email")
	private String mailId;
	private String gender;
	private String address;
}

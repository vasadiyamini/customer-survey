package com.survey.app.service;

import java.util.List;

import com.survey.app.entity.Survey;

public interface SurveyService {
	//List<Survey> getCus(int customerId, int branchId);

	List<Survey> getCus(int branchId, int distance);

}

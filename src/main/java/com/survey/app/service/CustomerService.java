package com.survey.app.service;

import com.survey.app.dto.ApiResponse;
import com.survey.app.dto.CustomerDto;

import jakarta.validation.Valid;

public interface CustomerService {

	ApiResponse addcustomer(@Valid CustomerDto customerDto);

}

package com.survey.app.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.survey.app.entity.Customer;



public interface CustomerRepository  extends JpaRepository<Customer, Integer>{
	Optional<Customer> findByMailId(String email);
}

package com.survey.app.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.survey.app.entity.Survey;

public interface SurveyRepository  extends JpaRepository<Survey, Integer>{



	//List<Survey> findByBranchbrachIdAndCustomercustomerId(int branchId, int customerId);

	List<Survey> findByBranchBranchIdAndDistanceLessThan(int branchId, int distance);
         
	
}

package com.survey.app.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.survey.app.entity.Customer;

public interface BranchRepository extends JpaRepository<Customer, Integer> {

}

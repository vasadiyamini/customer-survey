package com.survey.app.serviceimp;

import java.util.List;

import org.springframework.stereotype.Service;

import com.survey.app.entity.Survey;
import com.survey.app.exceptions.ResourceNotFound;
import com.survey.app.repository.SurveyRepository;
import com.survey.app.service.SurveyService;
@Service
public class SurveyServiceImp implements SurveyService {
	
private final SurveyRepository surveyRepository;
public SurveyServiceImp(SurveyRepository surveyRepository) {
	this.surveyRepository = surveyRepository;
}

	@Override
	public List<Survey> getCus( int branchId,int distance) {
		
		List<Survey> sur=surveyRepository.findByBranchBranchIdAndDistanceLessThan( branchId, distance);
		if(sur.isEmpty())
			throw new ResourceNotFound("no customer avilable with in this distance");
		return sur  ;
		
	}

}

package com.survey.app.serviceimp;

import java.util.Optional;

import org.springframework.stereotype.Service;

import com.survey.app.dto.ApiResponse;
import com.survey.app.dto.CustomerDto;
import com.survey.app.entity.Customer;
import com.survey.app.exceptions.ResourceNotFound;
import com.survey.app.repository.CustomerRepository;
import com.survey.app.service.CustomerService;

import jakarta.validation.Valid;
@Service
public class CustomerServiceImp implements CustomerService {
private final CustomerRepository customerRepository;

public CustomerServiceImp(CustomerRepository customerRepository) {
	this.customerRepository = customerRepository;
}

	@Override
	public ApiResponse addcustomer(@Valid CustomerDto customerDto) {
		Optional <Customer> customer = customerRepository.findByMailId(customerDto.getMailId());
		if(customer.isPresent()) {
			throw new ResourceNotFound("customer already registred");
		}
		Customer cus = Customer.builder().address(customerDto.getAddress()).firstName(customerDto.getFirstName())
				.gender(customerDto.getGender()).lastName(customerDto.getLastName()).build();
		
		customerRepository.save(cus);
		ApiResponse api = new ApiResponse();
		api.setStatus(200l);
		api.setMessage("Sucessfully customer added");
		return api;
		
	}

}

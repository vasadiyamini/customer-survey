package com.survey.app.exceptions;

import org.springframework.http.HttpStatus;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class CustomerSurveyGlobalException extends RuntimeException{
	private static final long serialVersionUID = 1L;
	private String message;
	private HttpStatus status;
	
	
	public CustomerSurveyGlobalException(String message) {
		super();
		this.message = message;
	}


}

package com.survey.app.service;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import com.survey.app.dto.ApiResponse;
import com.survey.app.dto.CustomerDto;
import com.survey.app.entity.Customer;
import com.survey.app.exceptions.ResourceNotFound;
import com.survey.app.repository.CustomerRepository;
import com.survey.app.serviceimp.CustomerServiceImp;

class CustomerServiceTest {

	
		@Mock
	    private CustomerRepository customerRepository;
	    @InjectMocks
	    private CustomerServiceImp customerService;
	    @Test
	    void testAddCustomer_Success() {
	        CustomerDto customerDto = new CustomerDto();
	        customerDto.setAddress("123 Main St");
	        customerDto.setFirstName("John");
	        customerDto.setLastName("Doe");
	        customerDto.setGender("Male");
	        customerDto.setMailId("john@example.com");
	        when(customerRepository.findByMailId("john@example.com")).thenReturn(Optional.empty());
	        ApiResponse response = customerService.addcustomer(customerDto);
	        assertEquals(200L, response.getStatus());
	        assertEquals("Successfully customer added", response.getMessage());
	        verify(customerRepository, times(1)).save(any(Customer.class));
	    }
	
	    @Test
	    void testAddCustomer_Failure_CustomerAlreadyRegistered() {
	        CustomerDto customerDto = new CustomerDto();
	        customerDto.setAddress("123 Main St");
	        customerDto.setFirstName("John");
	        customerDto.setLastName("Doe");
	        customerDto.setGender("Male");
	        customerDto.setMailId("john@example.com");
	        when(customerRepository.findByMailId("john@example.com")).thenReturn(Optional.of(new Customer()));
	        assertThrows(ResourceNotFound.class, () -> customerService.addcustomer(customerDto));
	        verify(customerRepository, never()).save(any(Customer.class));
	    }
	    @Test
	    void testAddCustomer_NullCustomerDto() {
	        assertThrows(IllegalArgumentException.class, () -> customerService.addcustomer(null));
	        verify(customerRepository, never()).findByMailId(anyString());
	        verify(customerRepository, never()).save(any(Customer.class));
	    }
	    @Test
	    void testAddCustomer_NullMailId() {
	        CustomerDto customerDto = new CustomerDto();
	        customerDto.setAddress("123 Main St");
	        customerDto.setFirstName("John");
	        customerDto.setLastName("Doe");
	        customerDto.setGender("Male");
	        assertThrows(IllegalArgumentException.class, () -> customerService.addcustomer(customerDto));
	        verify(customerRepository, never()).findByMailId(anyString());
	        verify(customerRepository, never()).save(any(Customer.class));
	    }
	    @Test
	    void testAddCustomer_EmptyMailId() {
	        CustomerDto customerDto = new CustomerDto();
	        customerDto.setAddress("123 Main St");
	        customerDto.setFirstName("John");
	        customerDto.setLastName("Doe");
	        customerDto.setGender("Male");
	        customerDto.setMailId("");
	        assertThrows(IllegalArgumentException.class, () -> customerService.addcustomer(customerDto));
	        verify(customerRepository, never()).findByMailId(anyString());
	        verify(customerRepository, never()).save(any(Customer.class));
	    }
	

}

